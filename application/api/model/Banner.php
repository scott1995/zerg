<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/6
 * Time: 10:15
 */

namespace app\api\model;


use think\Db;
use think\Model;

class Banner extends Model
{
  protected $hidden = ['delete_time','update_time'];
  public function items()
  {
    return $this->hasMany('BannerItem','banner_id','id');
  }
  public static function getBannerByID($id)
  {
    $banner = self::with(['items','items.img'])->find($id);
    return $banner;
  }
}