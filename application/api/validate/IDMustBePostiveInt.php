<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/5
 * Time: 16:25
 */

namespace app\api\validate;


class IDMustBePostiveInt extends BaseValidate
{
  protected $rule = [
    'id' => 'require|isPositiveInteger'
  ];

  protected function isPositiveInteger($value,$rule='',
                                       $data='',$field='')
  {
    if(is_numeric($value) && ($value  > 0) ) {
      return true;
    } else{
      return $field.'必须是正整数';
    }
  }
}