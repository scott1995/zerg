<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/5
 * Time: 16:49
 */

namespace app\api\validate;


use think\Exception;
use think\Request;
use think\Validate;

class BaseValidate extends Validate
{
  public function goCheck()
  {
    //获取http参数，对参数做校验
    $params = Request::instance()->param();
    $result = $this->check($params);
    if(!$result){
     $error = $this->error;
     throw new Exception($error);
    } else{
      return true;
    }
  }
}