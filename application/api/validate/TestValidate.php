<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/5
 * Time: 16:02
 */

namespace app\api\validate;
use think\Validate;

class TestValidate extends Validate
{
  protected $rule = [
    'name' => 'require|max:10',
    'email' => 'email'
  ];
}