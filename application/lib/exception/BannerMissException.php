<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/6
 * Time: 13:20
 */

namespace app\lib\exception;


class BannerMissException extends BaseException
{
  public $code = 404;
  public $msg = '请求的Banner不存在';
  public $errorCode = 40000;
}